from django.apps import AppConfig


class AppTeveoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'App_TeVeO'
